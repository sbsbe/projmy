const options = {
    chart: {
        hight: 450,
        width: '100%',
        type: 'bar',
        foreColor: '#333'
    },
    series: [{
        name: 'population',
        data: [8550405, 3971883, 2720546, 2720546, 1567442, 1563025, 1469845, 1394928]
    }],
    xaxis: {
        categories: [ 
         'New York City',
         'Los Angeles', 
         'Chicago',
         'Houston', 
         'Philadelphia',
         'Phoenix', 
         'San Antonio', 
         'San Diego']},
         plotOptions: {bar: {horizontal:false}},
         fill: {colors: ['#f44336']},
         title: {text:'The largest US cities', align:'center', margin: 20, offsetY: 20, style:{fontSize: '25px'}}
};
const chart = new ApexCharts(document.querySelector('#chart'), options);
chart.render();

document.querySelector('button').addEventListener("click", ()
=> chart.updateOption({plotOptions: {bar: {horizontal:true}}}));